import React, { useState, useEffect } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Loader from 'react-loader-spinner'

import "./App.css";
import Main from "./components/pv/Main";
import Login from "./components/login/Index";

function App() {
  const settingsData = useSelector((state) => state.settings);
  const userData = useSelector((state) => state.user);
  const userId = localStorage.getItem("userId");
  const token = localStorage.getItem("token");
  const dispatch = useDispatch();
  useEffect(() => {
    // const formData = new FormData();
    // formData.append("userID", userId);
    // formData.append("token", token);
    dispatch({ type: "USER_REQUEST", payload: { userId, token } });
  }, []);
  return (
    <>
      {/* {userData.user ? <Redirect to="/" />:<Redirect to="/login" />} */}
      {/* {settingsData.settings.mode && <Darkmode /> } */}
        {
          userData.isLoader?
          <div style={{ margin: "20% 47%" }}>
          <Loader
          type="Oval"
          color="#00BFFF"
          height={100}
          width={100}
          />
         </div>
         :
         <Switch>
         <Route path="/login">
           <Login />
         </Route>
         <Route exact path="/">
           <Main />
         </Route>
       </Switch>
        }
    </>
  );
}

export default App;
