import React, { createContext, useReducer } from "react";

const initialState = {
  isTyping: false,
  did: 0
};

const pvReducer = (state,action) => {
    switch(action.type){
        case "ADD_TYPING":
            return {
                isTyping: true,
                did: action.payload
              }
        case "REMOVE_TYPING":
            return {
                isTyping: false,
                did: action.payload
                }
        default:
            return state;
    }
}

export const PvContext = createContext();

const PvContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(pvReducer, initialState);
  return (
    <PvContext.Provider value={{ state, dispatch }}>
      {children}
    </PvContext.Provider>
  );
};

export default PvContextProvider;
