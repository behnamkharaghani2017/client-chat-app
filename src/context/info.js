import React, { createContext, useReducer } from "react";

const initialState = {
  mode: false,
  image: '',
};



const infoReducer = (info,action) => {
    switch(action.type){
        case "mode":
            return {
              ...info,
                mode: action.payload,
              }
        case "image":
          return {
            ...info,
              image: action.payload,
            }
        case "UPDATE_INFO":
          return {
            ...info,
              image: action.payload.image,
              mode: action.payload.mode,
            }
        default:
            return info;
    }
}

export const InfoContext = createContext();

const InfoContextProvider = ({ children }) => {
  const [info, dispatch] = useReducer(infoReducer, initialState);
  return (
    <InfoContext.Provider value={{ info, dispatch }}>
      {children}
    </InfoContext.Provider>
  );
};

export default InfoContextProvider;
