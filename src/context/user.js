import React from "react";

const userContext = React.createContext({
  data: {},
  login: false,
});

export default userContext;
