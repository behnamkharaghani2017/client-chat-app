import axios from 'axios';

class httpService{
    constructor(){
        this.client = axios.create({
            baseURL: 'http://localhost:4000/api/',
            timeout: 60000,
            headers: {}
          });
    }
    get(url, config = null){
        return this.client.get(url,config);
    }
    post(url,params,config = null){
        return this.client.post(url,params,config);
    }
    put(url,params,config=null){
        return this.client.put(url,params,config);
    }
    delete(url,config=null){
        return this.client.delete(url,config)
    }
}

export default httpService;