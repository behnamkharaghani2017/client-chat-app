import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Switch, Route, Redirect } from "react-router-dom";

import styles from "../../Login.module.css";
import httpService from "../../services/httpService";

export default function Login(props) {
  const userData = useSelector((state) => state.user);

  const login = useSelector((state) => state.login);
  const userId = localStorage.getItem("userId");
  const token = localStorage.getItem("token");
  const loginDispatch = useDispatch();







  const [num, setNum] = useState("");
  useEffect(() => {
    console.log(num);
  }, [num]);
  async function doLogin(e) {
    e.preventDefault();
    const mobile = e.target.mobile.value;
    // const httpServ = new httpService();
    // const res = await httpServ.post("v1/auth/login", { mobile: mobile });
    // props.stateChanger();
    // props.parentCallback(mobile);
    // console.log(mobile);
    loginDispatch({type:"LOGIN_REQUEST",payload:{ mobile } })
  }

  return (
    <>
      {/* {userData.user && <Redirect to="/" />} */}
      <div>
        <div className={styles.auth_form}>
          <div className={styles.logo}></div>
          <h2>تلگرام</h2>
          <p className={styles.note}>
            {" "}
            لطفاً شماره تلفن خود را وارد کنید تا کد تائید برای شما ارسال گردد.
          </p>
          <form action onSubmit={doLogin}>
            <div className={`${styles.inputGroup} with-label touched`}>
              <input
                className={styles.formControl}
                type="text"
                placeholder="09** *** ****"
                maxlength="11"
                name="mobile"
                onKeyUp={(e) => {
                  const value = e.target.value;
                  console.log(value);
                  if (!isNaN(parseInt(value)) && value.length < 12) {
                    setNum(value);
                  }
                }}
                id="sign-in-phone-number"
                dir="auto"
                inputmode="tel"
              />
              <label htmlFor="sign-in-phone-number">شماره تلفن شما</label>
            </div>
            {console.log(num)}
            {num.length == 11 && (
              <button type="submit" className={styles.Button}>
                ادامه<div className="ripple-container"></div>
              </button>
            )}
          </form>
        </div>
      </div>
    </>
  );
}
