import React, { useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";

import styles from "../../Verify.module.css";
import { Redirect } from "react-router-dom";
import httpService from "../../services/httpService";


export default function Verify(props) {
  const login = useSelector((state) => state.login);

  const verify = useSelector((state) => state.verify);
  const userId = localStorage.getItem("userId");
  const token = localStorage.getItem("token");
  const verifyDispatch = useDispatch();
  const userDispatch = useDispatch();

  const mobile = login.login.newUser.mobile;
  console.log('mobile',login.login.newUser.mobile)
  const [red, setRed] = useState(false);

  const verifyCode = async (e) => {
    const code = e.target.value;
    // const mobile = props.val;
    console.log(mobile);
    if (code.length === 5) {
      // const httpServ = new httpService();
      // const res = await httpServ.post("v1/auth/verify", { mobile, code });
      verifyDispatch({type:"VERIFY_REQUEST",payload:{ mobile, code } })
      // console.log(res);
      // if (verify) {
    
      //   setRed(true);
      //   console.log(verify)
      // }
    }
  };
  // verify.status && userDispatch({type:"",payload:{}})
  console.log('login1',verify)
  return (
    <div>
      <div id="UiLoader">
        {verify.status && <Redirect to="/" />}
        <div id={styles.authCodeForm} className="custom-scroll">
          {/* <input type="hidden" ref={mobile=props.val} />  */}
          <div className={styles.authForm}>
            {/* <div id="monkey" className="">
                        <div className="AnimatedSticker" style={{width: 160, height: 160}}>
                        <canvas width="200" height="200" style={{width: 160, height: 160}}></canvas>
                        </div>
                        <div className="AnimatedSticker hidden" style={{width: 160, height: 160}}>
                        <canvas width="200" height="200" style={{width: 160, height: 160}}></canvas>
                        </div>
                    </div> */}
            <h2>
              <span>{props.val}</span>
              <div
                onClick={props.stateChanger}
                className={styles.authNumberEdit}
                tabIndex="0"
                title="Wrong number?"
              >
                <i className="fal fa-pen"></i>
              </div>
            </h2>
            <p className={styles.note}>
              ما کد را از طریق پیامک برای شما ارسال کرده ایم.
            </p>
            <div className={`${styles.inputGroup} with-label touched`}>
              <input
                className={styles.formControl}
                type="text"
                name="code"
                onKeyUp={verifyCode}
                id="sign-in-code" 
                dir="auto"
                autoComplete="one-time-code"
                inputmode="numeric"
              />
              <label htmlFor="sign-in-code">کد تائید</label>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
} 
