import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import Login from './Login'
import Verify from './Verify'
export default function Index() {
  const userData = useSelector((state) => state.user);
  const loginData = useSelector((state) => state.login);
    console.log('index',userData.user )
  return (
    <>
      {userData.user && <Redirect to="/" />}
      {JSON.stringify(userData)}
      {
          loginData.status?
          <Verify />
          :
          <Login />
      }
    </>
  );
}
