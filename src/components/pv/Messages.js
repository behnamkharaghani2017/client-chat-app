import React from "react";

export default function Messages({ message, id, user }) {
  let m;
  if (message.type === "msg") {
    m = message.message;
  } else if (message.type === "voice") {
    m = (
      <audio controls>
        <source src={message.message} type={"audio/mpeg"} />
      </audio>
    );
  } else if (message.type === "file") {
  }

  return (
    <li className={id == message.sender && "user_direct"}>
      <img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" />
      <div className="content">
        <div className="message">
          <div className="bubble">
            <p>{m}</p>
          </div>
          <div style={{ display: "flex", alignItems: "baseline" }}>
          {message.sender === user.id ? (
            message.seen === "0" ? (
              <i class="fal fa-check"></i>
            ) : (
              <i
                style={{ color: message.seen === "2" && "#1e90ff" }}
                class="fal fa-check-double"
              ></i>
            )
          ) : (
            ""
          )}
          <span>{message.updatedAt}</span>
        </div>
        </div>
      </div>
    </li>
  );
}
