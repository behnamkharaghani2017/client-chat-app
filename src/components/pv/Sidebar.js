import React, { useEffect, useState, useRef, useContext } from "react";
// import { useSelector, useDispatch } from "react-redux";
import httpService from "../../services/httpService";

// import {
//     BrowserRouter as Router,
//     Switch,
//     Route,
//     Link
//   } from "react-router-dom";

import Conversations from "./sidebar/Conversations";
import Friends from "./sidebar/Friends";
import Notifications from "./sidebar/Notifications";
import Settings from "./sidebar/Settings";
// import UserContext from "../../context/user";
// import { InfoContext } from "../../context/info";

export default function Sidebar(props) {
  // const userId = useContext(UserContext);
  // const [users, setUsers] = useState([]);


  // const {info,dispatch} = useContext(InfoContext);
  // console.log(info)
 

  useEffect(() => {
    // usersDispatch({type:"USERS_REQUEST"})
    // console.log(users)
    // const httpServ = new httpService();
    // const res = httpServ
    //   .post("v1/users")
    //   .then(function (response) {
    //     const idd = localStorage.getItem("user");
    //     const user = response.data.filter((item) => item.id != idd);
    //     setUsers(user);
    //   })
    //   .catch(function (error) {
    //     // console.log('errr',error);
    //   });
  }, []);

  return (
    <>
      <div className="sidebar">
        <div className="container">
          <div className="tab-content">
            <Conversations />
            <Friends />
            <Notifications />
            <Settings />
          </div>
        </div>
      </div>
    </>
  );
}
