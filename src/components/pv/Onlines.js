import React,{useContext} from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink
  } from "react-router-dom";
import { io } from "socket.io-client";
import UserContext from '../../context/user'
export default function Onlines(props) {
    const usera = useContext(UserContext);
    // console.log(usera)
    const {user,userId} = props;
    // console.log(props)
    const socket = io("http://localhost:4000");
    socket.on(`direct online${userId}`,({ sender, receiver})=>{
        console.log('answer online', sender, receiver)
      })
    socket.on(`disconnect${userId}`,({ socket})=>{
        console.log('answer disconnect', socket)
      })
    return (
        			<li>
                        <NavLink to={`/pv/${user.id}`} className={"filter direct active"} className={"filter direct"} data-chat="open" data-toggle="tab" role="tab" aria-controls="chat1" aria-selected="true">
                            <div className="status online">
                                <img src="/dist/img/avatars/avatar-male-1.jpg" alt="avatar" /><i data-eva="radio-button-on"></i></div>
                            <div className="content">
                                <div className="headline">
                                    <h5>{`${user.firstName} ${user.lastName}`}</h5>
                                    <span>امروز</span>
                                </div>
                                <p>سلام ممنون از شما.</p>
                            </div>
                        </NavLink>
                        </li>  
                       
    )
}
