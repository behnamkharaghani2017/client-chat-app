import React, { useState, useEffect, useRef, useContext } from "react";
import { ReactMic } from "react-mic";
import axios from "axios";

import Typing from "./Typing";

import { PvContext } from "../../context/pv";

export default function MessageInput({ direct, user, socket }) {
  const [record, setRecord] = useState(false);
  const [attachment, setAttachment] = useState();
  const inputFileRef = useRef();

  // const {state,dispatch} = useContext(PvContext);

  const userRef = useRef();
  const startRecordVoice = () => {
    if (record) setRecord(false);
    else setRecord(true);
  };
  const attachFile = () => {
    inputFileRef.current.click();
  };
  const onChangeFile = (e) => {
    const files = e.target.files;
    if (files && files.length > 0) {
      setAttachment(files[0]);
      const data = new FormData();
      data.append("file", files[0]);
      axios
        .post("http://localhost:4000/api/v1/uploadFile", data)
        .then((res) => {
          const filePath = res.data.filePath;
          socket.current.emit("uploadFile", {
            path: filePath,
            sender: {
              name: user.id,
              // gender: props.location.state.gender,
            },
            receiver: {
              name: direct,
            },
          });
        })
        .catch((err) => {
          alert("فایل شما ارسال نشد متاسفانه");
        });
    }
  };
  const onData = (recordedBlob) => {};

  const onStop = (recordedBlob) => {
    console.log("recordedBlob is: ", recordedBlob);
    const formData = new FormData();
    formData.append("voiceMessage", recordedBlob.blob);
    axios
      .post("http://localhost:4000/api/v1/uploadVoice", formData)
      .then((res) => {
        console.log({
          filePath: res.data.filePath,
          sender: {
            name: user.id,
            // gender: props.location.state.gender,
          },
          receiver: {
            name: direct,
          },
        });
        socket.emit("uploadVoice", {
          path: res.data.filePath,
          sender: {
            name: user.id,
            // gender: props.location.state.gender,
          },
          receiver: {
            name: direct,
          },
        });
      })
      .catch((err) => {
        alert("ارسال نشد ویس شما");
      });
  };

  const typeHandler = (e) => {
    socket.emit("istyping", {directId:direct,userMeta:user});
  };
  let lastUpdateTime;
  useEffect(() => {
    socket.on(`istyping${user.id}`, ({directId,userMeta}) => {
      if(direct==userMeta.id){
        lastUpdateTime = Date.now();
        // if (!state.isTyping) {
        //   dispatch({type:"ADD_TYPING",payload:userMeta.id})
        //   let typingInterval = setInterval(() => {
        //     if (Date.now() - lastUpdateTime > 2000) {
        //       dispatch({type:"REMOVE_TYPING",payload:userMeta.id})
        //       if (typingInterval) {
        //         clearInterval(typingInterval);
        //       }
        //     }
        //   }, 2000);
        // }
      }
   
    });
  }, []);

  const sendMessage = (e) => {
    e.preventDefault();
    const msg = e.currentTarget.elements.msg_text.value;
    socket.emit("new chat", { msg, id: direct, userId: user });
    e.currentTarget.elements.msg_text.value = "";
  };
  return (
    <div className="container">
      <div className={`${record ? "recordActive" : ""} bottom`}>
        <form
          onSubmit={sendMessage}
          style={{ display: "flex", alignItems: "center" }}
        >
          <div className="microVoice">
            <i className="fal fa-microphone-alt" onClick={startRecordVoice}></i>
          </div>
          <div className="soundWaves">
            <ReactMic
              record={record}
              className="soundWave"
              onStop={onStop}
              onData={onData}
              strokeColor="#000000"
              backgroundColor="#FFffff"
            />
          </div>
          <div className="textBoxPv">
            <textarea
              className="form-control"
              onChange={typeHandler}
              placeholder="پیام را تایپ کنید ..."
              id="msg_text"
              rows="1"
            ></textarea>
            <button type="submit" className="btn prepend">
              <i className="fal fa-paper-plane" data-eva="paper-plane"></i>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
