import React from 'react'

export default function Group() {
    return (
        <div className="tab-pane fade" id="chat2" role="tabpanel">
        <div className="item">
            <div className="content">
                <div className="container">
                    <div className="top">
                        <div className="headline">
                            <img src="/dist/img/avatars/avatar-group-1.jpg" alt="avatar" />
                            <div className="content">
                                <h5>The Musketeers</h5>
                                <span>Group discussion</span>
                            </div>
                        </div>
                        <ul>
                            <li><button type="button" className="btn"><i data-eva="video" data-eva-animation="pulse"></i></button></li>
                            <li><button type="button" className="btn"><i data-eva="phone" data-eva-animation="pulse"></i></button></li>
                            <li><button type="button" className="btn" data-toggle="modal" data-target="#compose"><i data-eva="person-add" data-eva-animation="pulse"></i></button></li>
                            <li><button type="button" className="btn" data-utility="open"><i data-eva="info" data-eva-animation="pulse"></i></button></li>
                            <li><button type="button" className="btn round" data-chat="open"><i data-eva="arrow-ios-back"></i></button></li>
                            <li><button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical" data-eva-animation="pulse"></i></button>
                                <div className="dropdown-menu">
                                    <button type="button" className="dropdown-item"><i data-eva="video"></i>Video call</button>
                                    <button type="button" className="dropdown-item"><i data-eva="phone"></i>Voice call</button>
                                    <button type="button" className="dropdown-item" data-toggle="modal" data-target="#compose"><i data-eva="person-add"></i>Add people</button>
                                    <button type="button" className="dropdown-item" data-utility="open"><i data-eva="info"></i>Information</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="middle">
                    <div className="container">
                        <ul>
                            <li>
                                <img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" />
                                <div className="content">
                                    <div className="message">
                                        <div className="bubble">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                    <span>07:30am</span>
                                </div>
                            </li>
                            <li>
                                <img src="/dist/img/avatars/avatar-male-5.jpg" alt="avatar" />
                                <div className="content">
                                    <div className="message">
                                        <div className="bubble">
                                            <p>Many desktop publishing packages.</p>
                                        </div>
                                    </div>
                                    <span>11:56am</span>
                                </div>
                            </li>
                            <li>
                                <img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" />
                                <div className="content">
                                    <div className="message">
                                        <div className="bubble">
                                            <p>It has survived not only five centuries, but also the leap into electronic typesetting.</p>
                                        </div>
                                    </div>
                                    <span>01:03pm</span>
                                </div>
                            </li>
                            <li>
                                <img src="/dist/img/avatars/avatar-male-5.jpg" alt="avatar" />
                                <div className="content">
                                    <div className="message">
                                        <div className="bubble">
                                            <p>It was popularised in the 1960s.</p>
                                        </div>
                                    </div>
                                    <span>05:42pm</span>
                                </div>
                            </li>
                            <li>
                                <img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" />
                                <div className="content">
                                    <div className="message">
                                        <div className="bubble">
                                            <p>It is a long established fact that a reader will be distracted.</p>
                                        </div>
                                    </div>
                                    <span>08:20pm</span>
                                </div>
                            </li>
                            <li>
                                <img src="/dist/img/avatars/avatar-male-5.jpg" alt="avatar" />
                                <div className="content">
                                    <div className="message">
                                        <div className="bubble">
                                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text.</p>
                                        </div>
                                    </div>
                                    <span>10:15pm <i data-eva="done-all"></i></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="container">
                    <div className="bottom">
                        <form>
                            <textarea className="form-control" placeholder="Type message..." rows="1"></textarea>
                            <button type="submit" className="btn prepend"><i data-eva="paper-plane"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <div className="utility">
                <div className="container">
                    <button type="button" className="close" data-utility="open"><i data-eva="close"></i></button>
                    <button type="button" className="btn primary" data-toggle="modal" data-target="#compose">Add people</button>
                    <ul className="nav" role="tablist">
                        <li><a href="#users2" className="active" data-toggle="tab" role="tab" aria-controls="users2" aria-selected="true">Users</a></li>
                        <li><a href="#files2" data-toggle="tab" role="tab" aria-controls="files2" aria-selected="false">Files</a></li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane fade active show" id="users2" role="tabpanel">
                            <h4>Users</h4>
                            <hr/>
                            <ul className="users">
                                <li>
                                    <div className="status online"><img src="/dist/img/avatars/avatar-male-1.jpg" alt="avatar" /><i data-eva="radio-button-on"></i></div>
                                    <div className="content">
                                        <h5>Ham Chuwon</h5>
                                        <span>Florida, US</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="status offline"><img src="/dist/img/avatars/avatar-male-2.jpg" alt="avatar" /><i data-eva="radio-button-on"></i></div>
                                    <div className="content">
                                        <h5>Quincy Hensen</h5>
                                        <span>Shanghai, China</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="status online"><img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" /><i data-eva="radio-button-on"></i></div>
                                    <div className="content">
                                        <h5>Mark Hog</h5>
                                        <span>Olso, Norway</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="status offline"><img src="/dist/img/avatars/avatar-male-4.jpg" alt="avatar" /><i data-eva="radio-button-on"></i></div>
                                    <div className="content">
                                        <h5>Sanne Viscaal</h5>
                                        <span>Helena, Montana</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="status offline"><img src="/dist/img/avatars/avatar-male-5.jpg" alt="avatar" /><i data-eva="radio-button-on"></i></div>
                                    <div className="content">
                                        <h5>Alex Just</h5>
                                        <span>London, UK</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="status online"><img src="/dist/img/avatars/avatar-male-6.jpg" alt="avatar" /><i data-eva="radio-button-on"></i></div>
                                    <div className="content">
                                        <h5>Arturo Thomas</h5>
                                        <span>Vienna, Austria</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        
                        <div className="tab-pane fade" id="files2" role="tabpanel">
                            <h4>Files</h4>
                            <div className="upload">
                                <label>
                                    <input type="file" />
                                    <span>Drag & drop files here</span>
                                </label>
                            </div>
                            <ul className="files">
                                <li>
                                    <ul className="avatars">
                                        <li><button className="btn round"><i data-eva="file-text"></i></button></li>
                                        <li><a href="#"><img src="/dist/img/avatars/avatar-male-1.jpg" alt="avatar" /></a></li>
                                    </ul>
                                    <div className="meta">
                                        <a href="#"><h5>workbox.js</h5></a>
                                        <span>2kb</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <ul className="avatars">
                                        <li><button className="btn round"><i data-eva="folder"></i></button></li>
                                        <li><a href="#"><img src="/dist/img/avatars/avatar-male-2.jpg" alt="avatar" /></a></li>
                                    </ul>
                                    <div className="meta">
                                        <a href="#"><h5>bug_report</h5></a>
                                        <span>1kb</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <ul className="avatars">
                                        <li><button className="btn round"><i data-eva="briefcase"></i></button></li>
                                        <li><a href="#"><img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" /></a></li>
                                    </ul>
                                    <div className="meta">
                                        <a href="#"><h5>nuget.zip</h5></a>
                                        <span>7mb</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <ul className="avatars">
                                        <li><button className="btn round"><i data-eva="image-2"></i></button></li>
                                        <li><a href="#"><img src="/dist/img/avatars/avatar-male-4.jpg" alt="avatar" /></a></li>
                                    </ul>
                                    <div className="meta">
                                        <a href="#"><h5>clearfix.jpg</h5></a>
                                        <span>1kb</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <ul className="avatars">
                                        <li><button className="btn round"><i data-eva="folder"></i></button></li>
                                        <li><a href="#"><img src="/dist/img/avatars/avatar-male-5.jpg" alt="avatar" /></a></li>
                                    </ul>
                                    <div className="meta">
                                        <a href="#"><h5>package</h5></a>
                                        <span>4mb</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <ul className="avatars">
                                        <li><button className="btn round"><i data-eva="file-text"></i></button></li>
                                        <li><a href="#"><img src="/dist/img/avatars/avatar-male-6.jpg" alt="avatar" /></a></li>
                                    </ul>
                                    <div className="meta">
                                        <a href="#"><h5>plugins.js</h5></a>
                                        <span>3kb</span>
                                    </div>
                                    <div className="dropdown">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-eva="more-vertical"></i></button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <button type="button" className="dropdown-item">Edit</button>
                                            <button type="button" className="dropdown-item">Share</button>
                                            <button type="button" className="dropdown-item">Delete</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}
