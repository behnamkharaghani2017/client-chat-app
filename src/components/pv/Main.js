import React, { useEffect, useState } from "react";
import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { io } from "socket.io-client";
// import Loader from "react-loader-spinner";

import httpService from "../../services/httpService";
import Navigation from "./Navigation";
import Sidebar from "./Sidebar";
import Chat from "./Chat";

import UserContext from "../../context/user";

export default function Main() {
  const userData = useSelector((state) =>state.user);

  const [video, setVideo] = useState("none");
  const [confirmed, setConfirmed] = useState(false);
  const [load, setLoad] = useState(false);
  const [data, setData] = useState(0);
  const [user, setUser] = useState({});
  // const [online,setOnline] = useState([])
  const socket = io("http://localhost:4000");
  const { RTCPeerConnection, RTCSessionDescription } = window;
  const peerConnection = new RTCPeerConnection();

  async function callUser(socketId) {
    // console.log("eeeeee");
    const offer = await peerConnection.createOffer();
    await peerConnection.setLocalDescription(new RTCSessionDescription(offer));
    socket.emit("call-user", {
      offer,
      to: socketId,
    });
  }
  socket.on("call-made", async (data) => {
    // const confirmed = confirm(
    //     `کاربر با شناسه ${data.socket} می خواهد با شما تماس بگیرد . قبول می نماِیید؟`
    // );
    // console.log("rrrrr");
    document.querySelector(".video-container").style.display = "block";
    if (!confirmed) {
      socket.emit("reject-call", {
        from: data.socket,
      });

      return;
    }

    await peerConnection.setRemoteDescription(
      new RTCSessionDescription(data.offer)
    );

    const answer = await peerConnection.createAnswer();

    await peerConnection.setLocalDescription(new RTCSessionDescription(answer));

    socket.emit("make-answer", {
      answer,
      to: data.socket,
    });
  });

  // navigator.getUserMedia({video:true,audio:true},stream =>{
  //     const localVideo = document.getElementById("local-video");
  //     if(localVideo){
  //         localVideo.srcObject = stream
  //     }
  // },
  // (eeror) =>{
  //     console.log(eeror)
  // })
  useEffect(() => {
    const token = localStorage.getItem("token");
    const userd = localStorage.getItem("user");
    const httpServ = new httpService();
    const res = httpServ
      .post("v1/auth/checktoken", { userd })
      .then(function (response) {
        const { id, firstName, lastName, code } = response.data.user;
        setUser({ ...user, id, firstName, lastName, code });
        setLoad(true);
      })
      .catch(function (error) {
        // console.log(error);
      });
  }, []);

  useEffect(() => {
    socket.on("connect", () => {
      const idu = localStorage.getItem("user");
      socket.emit("user online", { idu, id: socket.id });
      // console.log(socket.id)
    });

    socket.on("disconnect", () => {
      // console.log(socket.connected)
    });
  }, []);
  let online = [];
  socket.on("onliner", (users) => {
    // console.log('length',Object.keys(users).length)
    // for (const  key in users) {
    //     if(user.id != key ){
    //         console.log('lll',user.id,key,users[key])
    //         // setOnline([users[key]])
    //         online[key]=users.key;
    //     }
    //   }
    // console.log(users);
  });
  console.log('main',userData.user)
return(
  <>
   {JSON.stringify(userData)}
  {!userData.user && <Redirect to="/login" />}
  <div className="layout">
          <Navigation />
          <Sidebar
            online={[
              { id: 3, firstName: "سامان", lastName: "زمانی" },
              { id: 4, firstName: "سروش", lastName: "صالحی" },
            ]}
          />
          <Chat />
          <div className="video-container">
            <video
              autoPlay=""
              className="remote-video"
              id="remote-video"
            ></video>
            <video
              autoPlay=""
              muted=""
              className="local-video"
              id="local-video"
            ></video>
            <div>
              <button onClick={() => setConfirmed(true)}>تماس</button>
              <button onClick={callUser(socket.id)}>قطع تماس</button>
            </div>
          </div>
        </div>
  </>
)
  // if (load) {
    // return (

      // <UserContext.Provider value={user}>
      
      {/* </UserContext.Provider> */}
    // );
  // } else {
    // return (
      // <div style={{ margin: "20% 47%" }}>
        {/* <Loader
        type="Oval"
        color="#00BFFF"
        height={100}
        width={100}
        /> */}
      // </div>
    // );
  // }
}
