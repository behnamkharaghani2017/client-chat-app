import React, { useRef,useEffect } from "react";
import httpService from "../../../services/httpService";
import { useSelector, useDispatch } from "react-redux";
export default function Settings() {
    const settingsData = useSelector((state) =>state.settings);
    const userId = localStorage.getItem("user");
    // const users = usersData.users.filter((item) => item.id != userId);
    const settingsDispatch = useDispatch();
  
    useEffect(() => {
        const formData = new FormData();
        formData.append("userID", userId);
        settingsDispatch({ type: "SETTINGS_REQUEST" , payload: formData });
    }, []);


  const inputFile = useRef(null);
  const iconImageProfile = (e) => {
    inputFile.current.click();
  };
  const httpServices = new httpService();
  const imageProfile = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("image", e.target.files[0]);

    formData.append("userID", userId);
    // formData.append("userID1", 1);
    // formData.append("userID2", 22);
    // formData.append("userID3", 3);
    settingsDispatch({ type: "SETTINGS_REQUEST",payload: formData });
    // httpServices
    //   .post("/v1/uploadImage", formData)
    //   .then((response) => {
        // dispatch({type:"image",payload:response.data.filePath})
    //   })
    //   .catch((error) => {});
    // console.log(e.target.files[0])
  };
  return (
    <div className="settings tab-pane fade" id="settings" role="tabpanel">
      <div className="user">
        <label>
          <input type="file" />
          <img src="/dist/img/avatars/avatar-male-1.jpg" alt="avatar" />
        </label>
        <div className="content">
          <h5>Ham Chuwon</h5>
          <span>Florida, US</span>
        </div>
      </div>
      <h4>تنظیمات</h4>
      <ul id="preferences">
        <li>
          <a
            href="#"
            className="headline"
            data-toggle="collapse"
            aria-expanded="false"
            data-target="#account"
            aria-controls="account"
          >
            <div className="title">
              <h5>حساب کاربری</h5>
              <p>جزئیات پروفایل خود را به روز کنید</p>
            </div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-forward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-forward">
                  <rect
                    width="24"
                    height="24"
                    transform="rotate(-90 12 12)"
                    opacity="0"
                  ></rect>
                  <path d="M10 19a1 1 0 0 1-.64-.23 1 1 0 0 1-.13-1.41L13.71 12 9.39 6.63a1 1 0 0 1 .15-1.41 1 1 0 0 1 1.46.15l4.83 6a1 1 0 0 1 0 1.27l-5 6A1 1 0 0 1 10 19z"></path>
                </g>
              </g>
            </svg>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-downward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-downward">
                  <rect width="24" height="24" opacity="0"></rect>
                  <path d="M12 16a1 1 0 0 1-.64-.23l-6-5a1 1 0 1 1 1.28-1.54L12 13.71l5.36-4.32a1 1 0 0 1 1.41.15 1 1 0 0 1-.14 1.46l-6 4.83A1 1 0 0 1 12 16z"></path>
                </g>
              </g>
            </svg>
          </a>
          <div
            className="content collapse"
            id="account"
            data-parent="#preferences"
          >
            <div className="profileImage">
              <div className="circle">
                <img className="profile-pic" src={settingsData.settings.image} />
              </div>
              <div className="p-image">
                <i
                  className="fal fa-camera upload-button"
                  onClick={iconImageProfile}
                ></i>
                <input
                  className="file-upload"
                  name="imageProfile"
                  ref={inputFile}
                  onChange={imageProfile}
                  type="file"
                  accept="image/*"
                />
              </div>
            </div>

            <div className="inside">
              <form className="account">
                <div className="form-row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>نام</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="First name"
                        value="Ham"
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>نام خانوادگی</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Last name"
                        value="Chuwon"
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label>تلفن همراه</label>
                  <input
                    type="number"
                    className="form-control"
                    value=""
                  />
                </div>
                <div className="form-group">
                  <label>آدرس ایمیل</label>
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Enter your email address"
                    value="hamchuwon@gmail.com"
                  />
                </div>
                <div className="form-group">
                  <label>بیوگرافی</label>
                  <textarea
                    className="form-control"
                    placeholder="Tell us a little about yourself"
                  ></textarea>
                </div>
                <button type="submit" className="btn primary">
                  ذخیره تغییرات
                </button>
              </form>
            </div>
          </div>
        </li>

        <li>
          <a
            href="#"
            className="headline"
            data-toggle="collapse"
            aria-expanded="false"
            data-target="#privacy"
            aria-controls="privacy"
          >
            <div className="title">
              <h5>Privacy & Safety</h5>
              <p>Control your privacy settings</p>
            </div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-forward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-forward">
                  <rect
                    width="24"
                    height="24"
                    transform="rotate(-90 12 12)"
                    opacity="0"
                  ></rect>
                  <path d="M10 19a1 1 0 0 1-.64-.23 1 1 0 0 1-.13-1.41L13.71 12 9.39 6.63a1 1 0 0 1 .15-1.41 1 1 0 0 1 1.46.15l4.83 6a1 1 0 0 1 0 1.27l-5 6A1 1 0 0 1 10 19z"></path>
                </g>
              </g>
            </svg>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-downward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-downward">
                  <rect width="24" height="24" opacity="0"></rect>
                  <path d="M12 16a1 1 0 0 1-.64-.23l-6-5a1 1 0 1 1 1.28-1.54L12 13.71l5.36-4.32a1 1 0 0 1 1.41.15 1 1 0 0 1-.14 1.46l-6 4.83A1 1 0 0 1 12 16z"></path>
                </g>
              </g>
            </svg>
          </a>
          <div
            className="content collapse"
            id="privacy"
            data-parent="#preferences"
          >
            <div className="inside">
              <ul className="options">
                <li>
                  <div className="headline">
                    <h5>Safe Mode</h5>
                    <label className="switch">
                      <input type="checkbox" defaultChecked />
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
                <li>
                  <div className="headline">
                    <h5>History</h5>
                    <label className="switch">
                      <input type="checkbox" defaultChecked />
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
                <li>
                  <div className="headline">
                    <h5>Camera</h5>
                    <label className="switch">
                      <input type="checkbox" />
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
                <li>
                  <div className="headline">
                    <h5>Microphone</h5>
                    <label className="switch">
                      <input type="checkbox" />
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </li>

        <li>
          <a
            href="#"
            className="headline"
            data-toggle="collapse"
            aria-expanded="false"
            data-target="#alerts"
            aria-controls="alerts"
          >
            <div className="title">
              <h5>Notifications</h5>
              <p>Turn notifications on or off</p>
            </div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-forward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-forward">
                  <rect
                    width="24"
                    height="24"
                    transform="rotate(-90 12 12)"
                    opacity="0"
                  ></rect>
                  <path d="M10 19a1 1 0 0 1-.64-.23 1 1 0 0 1-.13-1.41L13.71 12 9.39 6.63a1 1 0 0 1 .15-1.41 1 1 0 0 1 1.46.15l4.83 6a1 1 0 0 1 0 1.27l-5 6A1 1 0 0 1 10 19z"></path>
                </g>
              </g>
            </svg>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-downward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-downward">
                  <rect width="24" height="24" opacity="0"></rect>
                  <path d="M12 16a1 1 0 0 1-.64-.23l-6-5a1 1 0 1 1 1.28-1.54L12 13.71l5.36-4.32a1 1 0 0 1 1.41.15 1 1 0 0 1-.14 1.46l-6 4.83A1 1 0 0 1 12 16z"></path>
                </g>
              </g>
            </svg>
          </a>
          <div
            className="content collapse"
            id="alerts"
            data-parent="#preferences"
          >
            <div className="inside">
              <ul className="options">
                <li>
                  <div className="headline">
                    <h5>Pop-up</h5>
                    <label className="switch">
                      <input type="checkbox" defaultChecked />
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
                <li>
                  <div className="headline">
                    <h5>Vibrate</h5>
                    <label className="switch">
                      <input type="checkbox" />
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
                <li>
                  <div className="headline">
                    <h5>Sound</h5>
                    <label className="switch">
                      <input type="checkbox" defaultChecked />
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
                <li>
                  <div className="headline">
                    <h5>Lights</h5>
                    <label className="switch">
                      <input type="checkbox" />
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </li>

        <li>
          <a
            href="#"
            className="headline"
            data-toggle="collapse"
            aria-expanded="false"
            data-target="#integrations"
            aria-controls="integrations"
          >
            <div className="title">
              <h5>Integrations</h5>
              <p>Sync your social accounts</p>
            </div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-forward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-forward">
                  <rect
                    width="24"
                    height="24"
                    transform="rotate(-90 12 12)"
                    opacity="0"
                  ></rect>
                  <path d="M10 19a1 1 0 0 1-.64-.23 1 1 0 0 1-.13-1.41L13.71 12 9.39 6.63a1 1 0 0 1 .15-1.41 1 1 0 0 1 1.46.15l4.83 6a1 1 0 0 1 0 1.27l-5 6A1 1 0 0 1 10 19z"></path>
                </g>
              </g>
            </svg>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-downward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-downward">
                  <rect width="24" height="24" opacity="0"></rect>
                  <path d="M12 16a1 1 0 0 1-.64-.23l-6-5a1 1 0 1 1 1.28-1.54L12 13.71l5.36-4.32a1 1 0 0 1 1.41.15 1 1 0 0 1-.14 1.46l-6 4.83A1 1 0 0 1 12 16z"></path>
                </g>
              </g>
            </svg>
          </a>
          <div
            className="content collapse"
            id="integrations"
            data-parent="#preferences"
          >
            <div className="inside">
              <ul className="integrations">
                <li>
                  <button className="btn round">
                    <i data-eva="google"></i>
                  </button>
                  <div className="content">
                    <div className="headline">
                      <h5>Google</h5>
                      <label className="switch">
                        <input type="checkbox" defaultChecked />
                        <span className="slider round"></span>
                      </label>
                    </div>
                    <span>Read, write, edit</span>
                  </div>
                </li>
                <li>
                  <button className="btn round">
                    <i data-eva="facebook"></i>
                  </button>
                  <div className="content">
                    <div className="headline">
                      <h5>Facebook</h5>
                      <label className="switch">
                        <input type="checkbox" defaultChecked />
                        <span className="slider round"></span>
                      </label>
                    </div>
                    <span>Read, write, edit</span>
                  </div>
                </li>
                <li>
                  <button className="btn round">
                    <i data-eva="twitter"></i>
                  </button>
                  <div className="content">
                    <div className="headline">
                      <h5>Twitter</h5>
                      <label className="switch">
                        <input type="checkbox" />
                        <span className="slider round"></span>
                      </label>
                    </div>
                    <span>No permissions set</span>
                  </div>
                </li>
                <li>
                  <button className="btn round">
                    <i data-eva="linkedin"></i>
                  </button>
                  <div className="content">
                    <div className="headline">
                      <h5>LinkedIn</h5>
                      <label className="switch">
                        <input type="checkbox" />
                        <span className="slider round"></span>
                      </label>
                    </div>
                    <span>No permissions set</span>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </li>

        <li>
          <a
            href="#"
            className="headline"
            data-toggle="collapse"
            aria-expanded="false"
            data-target="#appearance"
            aria-controls="appearance"
          >
            <div className="title">
              <h5>Appearance</h5>
              <p>Customize the look of Swipe</p>
            </div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-forward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-forward">
                  <rect
                    width="24"
                    height="24"
                    transform="rotate(-90 12 12)"
                    opacity="0"
                  ></rect>
                  <path d="M10 19a1 1 0 0 1-.64-.23 1 1 0 0 1-.13-1.41L13.71 12 9.39 6.63a1 1 0 0 1 .15-1.41 1 1 0 0 1 1.46.15l4.83 6a1 1 0 0 1 0 1.27l-5 6A1 1 0 0 1 10 19z"></path>
                </g>
              </g>
            </svg>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              class="eva eva-arrow-ios-downward"
            >
              <g data-name="Layer 2">
                <g data-name="arrow-ios-downward">
                  <rect width="24" height="24" opacity="0"></rect>
                  <path d="M12 16a1 1 0 0 1-.64-.23l-6-5a1 1 0 1 1 1.28-1.54L12 13.71l5.36-4.32a1 1 0 0 1 1.41.15 1 1 0 0 1-.14 1.46l-6 4.83A1 1 0 0 1 12 16z"></path>
                </g>
              </g>
            </svg>
          </a>
          <div
            className="content collapse"
            id="appearance"
            data-parent="#preferences"
          >
            <div className="inside">
              <ul className="options">
                <li>
                  <div className="headline">
                    <h5>Lights</h5>
                    <label className="switch">
                      <input type="checkbox" />
                      <span className="slider round mode"></span>
                    </label>
                  </div>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </li>
      </ul>
    </div>
  );
}
