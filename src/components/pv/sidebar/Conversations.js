import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Onlines from "../Onlines";

export default function Conversations() {
  const usersData = useSelector((state) => state.users);
  const userId = localStorage.getItem("userId");
  const users = usersData.users.filter((item) => item.id != userId);
  const usersDispatch = useDispatch();

  useEffect(() => {
    usersDispatch({ type: "USERS_REQUEST" });
  }, []);
  return (
    <div
      className="tab-pane fade show active"
      id="conversations"
      role="tabpanel"
    >
      <div className="top">
        <form>
          <input type="search" className="form-control" placeholder="جستجو" />
          <button type="submit" className="btn prepend">
            <i data-eva="search"></i>
          </button>
        </form>
      </div>
      <div className="middle">
        <h4>گفتگو ها</h4>
        <button
          type="button"
          className="btn round"
          data-toggle="modal"
          data-target="#compose"
        >
          <i data-eva="edit-2"></i>
        </button>
        <hr />

        <ul className="nav discussions" role="tablist">
          {users.length
            ? users.map((user) => <Onlines key={user.id} user={user} userId={userId}  />)
            : "isload"}
        </ul>
      </div>
    </div>
  );
}
