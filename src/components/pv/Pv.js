import React, {
  useEffect,
  useState,
  useContext,
  useRef,
} from "react";
import { io } from "socket.io-client";


import httpService from "../../services/httpService";
import Loader from "react-loader-spinner";
import UserContext from "../../context/user";
import MessageInput from './MessageInput'
import Messages from './Messages'
import Typing from "./Typing";
const AlwaysScrollToBottom = () => {
  const elementRef = useRef();
  useEffect(() => elementRef.current.scrollIntoView());
  return <div ref={elementRef} />;
};
export default function Pv(props) {
  const userId = useContext(UserContext);
  const [id, setId] = useState(props.match.params.id);
  const [udata, setUdata] = useState([]);
  const [chats, setChats] = useState([]);
  const [isload, setIsload] = useState(false);
  const [directOnline, setDirectOnline] = useState(0);
  const [onliner, setOnliner] = useState({})

  // const [newChat, setNewChat] = useState([]);

 

  const socket = io("http://localhost:4000");

  // useEffect(()=>{
  //     setIsload(true);
  // },[typing])

  // const sendUser = (e) => {
  //   e.preventDefault();
    // console.log("sendUser");
  // };



  useEffect(() => {
    // console.log(udata)
    const data = async () => {
      const id = props.match.params.id;
 
      const httpServ = new httpService();
      const res = await httpServ.post("/v1/pv", { id, pvId: userId.id });
      setUdata(res.data[0]);
      setChats(res.data[1]);
      setIsload(true);
      // console.log("pp", props);
  
      // console.log(res.data[1])
    };
    data();
    socket.emit('direct online',{sender: id,receiver:userId.id})
    socket.on(`direct online${userId.id}`,({ sender, receiver})=>{
      console.log(sender,receiver)
      console.log(userId.id,id)
      console.log('send')
        setDirectOnline(1)
        socket.emit(`direct online answer`,{sender: receiver,receiver:sender})

    })
    socket.on(`direct online${userId.id}`,({ sender, receiver})=>{
      console.log('answer')
      setDirectOnline(1)
    })
// console.log(`direct online${userId.id}`)
socket.on('onliner',({ids_socket})=>{
  console.log('ids_socket',ids_socket)
})
  }, []);
  const videoCall = () => {};



  socket.on("seenMessage", ({updatedRows}) => {
    // console.log('updatedRows',updatedRows)
   })
// console.log('ffff')
useEffect(()=>{
 
  socket.emit('seenMessage', {
    sender: id,
    receiver:userId.id,
    // id: message.id,
    // sender: props.location.state.name,
    // receiver: userRef.current,
  });
  socket.on("new chat", ({msgs,ids,userIds,doc}) => {
    if ((doc.sender == userIds )) {

    // console.log(userId.id)
    // if (doc.receiver === userId.id){
      // console.log(doc.receiver)
      // socket.emit('seenMessage', {
        // sender: id,
        // receiver:userId.id,
        // id: message.id,
        // sender: props.location.state.name,
        // receiver: userRef.current,
      // });
    // }
    setChats((chats) => chats.concat([doc]));
    }
  });
},[])
  if (id == userId.id) {
    return <div>چنین صفحه ای وجود ندارد.</div>;
  }
  if (isload) {
    return (
      <div className="tab-pane fade show active" id="chat1" role="tabpanel">
        <div className="item">
          <div className="content">
          <div class="container">
                <div class="top">
                  <div class="headline">
                  <img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" />
                  <div className="content">
                    <h5>{`${udata.firstName} ${udata.lastName}`}</h5>
                    <span>{directOnline===1?'آنلاین':'به تازگی بازدید شده'}</span>
                  </div>
                  </div>
                   <Typing dirId={id} />
                  <ul>
                    <li><button type="button" class="btn"><i class="eva-hover"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-video eva-animation eva-icon-hover-pulse"><g data-name="Layer 2"><g data-name="video"><rect width="24" height="24" opacity="0"></rect><path d="M21 7.15a1.7 1.7 0 0 0-1.85.3l-2.15 2V8a3 3 0 0 0-3-3H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h9a3 3 0 0 0 3-3v-1.45l2.16 2a1.74 1.74 0 0 0 1.16.45 1.68 1.68 0 0 0 .69-.15 1.6 1.6 0 0 0 1-1.48V8.63A1.6 1.6 0 0 0 21 7.15z"></path></g></g></svg></i></button></li>
                    <li><button type="button" class="btn"><i class="eva-hover"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-phone eva-animation eva-icon-hover-pulse"><g data-name="Layer 2"><g data-name="phone"><rect width="24" height="24" opacity="0"></rect><path d="M17.4 22A15.42 15.42 0 0 1 2 6.6 4.6 4.6 0 0 1 6.6 2a3.94 3.94 0 0 1 .77.07 3.79 3.79 0 0 1 .72.18 1 1 0 0 1 .65.75l1.37 6a1 1 0 0 1-.26.92c-.13.14-.14.15-1.37.79a9.91 9.91 0 0 0 4.87 4.89c.65-1.24.66-1.25.8-1.38a1 1 0 0 1 .92-.26l6 1.37a1 1 0 0 1 .72.65 4.34 4.34 0 0 1 .19.73 4.77 4.77 0 0 1 .06.76A4.6 4.6 0 0 1 17.4 22z"></path></g></g></svg></i></button></li>
                    <li><button type="button" class="btn" data-toggle="modal" data-target="#compose"><i class="eva-hover"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-person-add eva-animation eva-icon-hover-pulse"><g data-name="Layer 2"><g data-name="person-add"><rect width="24" height="24" opacity="0"></rect><path d="M21 6h-1V5a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0V8h1a1 1 0 0 0 0-2z"></path><path d="M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4z"></path><path d="M16 21a1 1 0 0 0 1-1 7 7 0 0 0-14 0 1 1 0 0 0 1 1"></path></g></g></svg></i></button></li>
                    <li><button type="button" class="btn" data-utility="open"><i class="eva-hover"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-info eva-animation eva-icon-hover-pulse"><g data-name="Layer 2"><g data-name="info"><rect width="24" height="24" transform="rotate(180 12 12)" opacity="0"></rect><path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm1 14a1 1 0 0 1-2 0v-5a1 1 0 0 1 2 0zm-1-7a1 1 0 1 1 1-1 1 1 0 0 1-1 1z"></path></g></g></svg></i></button></li>
                    <li><button type="button" class="btn round" data-chat="open"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-arrow-ios-back"><g data-name="Layer 2"><g data-name="arrow-ios-back"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"></rect><path d="M13.83 19a1 1 0 0 1-.78-.37l-4.83-6a1 1 0 0 1 0-1.27l5-6a1 1 0 0 1 1.54 1.28L10.29 12l4.32 5.36a1 1 0 0 1-.78 1.64z"></path></g></g></svg></button></li>
                    <li class=""><button type="button" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="eva-hover"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-more-vertical eva-animation eva-icon-hover-pulse"><g data-name="Layer 2"><g data-name="more-vertical"><rect width="24" height="24" transform="rotate(-90 12 12)" opacity="0"></rect><circle cx="12" cy="12" r="2"></circle><circle cx="12" cy="5" r="2"></circle><circle cx="12" cy="19" r="2"></circle></g></g></svg></i></button>
                      <div class="dropdown-menu" x-placement="bottom-start" style={{position: 'absolute', willChange: 'transform', top: 0,left: 0, transform: 'translate3d(0px, 5px, 0px)'}} x-out-of-boundaries="">
                        <button type="button" class="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-video"><g data-name="Layer 2"><g data-name="video"><rect width="24" height="24" opacity="0"></rect><path d="M21 7.15a1.7 1.7 0 0 0-1.85.3l-2.15 2V8a3 3 0 0 0-3-3H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h9a3 3 0 0 0 3-3v-1.45l2.16 2a1.74 1.74 0 0 0 1.16.45 1.68 1.68 0 0 0 .69-.15 1.6 1.6 0 0 0 1-1.48V8.63A1.6 1.6 0 0 0 21 7.15z"></path></g></g></svg>Video call</button>
                        <button type="button" class="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-phone"><g data-name="Layer 2"><g data-name="phone"><rect width="24" height="24" opacity="0"></rect><path d="M17.4 22A15.42 15.42 0 0 1 2 6.6 4.6 4.6 0 0 1 6.6 2a3.94 3.94 0 0 1 .77.07 3.79 3.79 0 0 1 .72.18 1 1 0 0 1 .65.75l1.37 6a1 1 0 0 1-.26.92c-.13.14-.14.15-1.37.79a9.91 9.91 0 0 0 4.87 4.89c.65-1.24.66-1.25.8-1.38a1 1 0 0 1 .92-.26l6 1.37a1 1 0 0 1 .72.65 4.34 4.34 0 0 1 .19.73 4.77 4.77 0 0 1 .06.76A4.6 4.6 0 0 1 17.4 22z"></path></g></g></svg>Voice call</button>
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target="#compose"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-person-add"><g data-name="Layer 2"><g data-name="person-add"><rect width="24" height="24" opacity="0"></rect><path d="M21 6h-1V5a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0V8h1a1 1 0 0 0 0-2z"></path><path d="M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4z"></path><path d="M16 21a1 1 0 0 0 1-1 7 7 0 0 0-14 0 1 1 0 0 0 1 1"></path></g></g></svg>Add people</button>
                        <button type="button" class="dropdown-item" data-utility="open"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="eva eva-info"><g data-name="Layer 2"><g data-name="info"><rect width="24" height="24" transform="rotate(180 12 12)" opacity="0"></rect><path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm1 14a1 1 0 0 1-2 0v-5a1 1 0 0 1 2 0zm-1-7a1 1 0 1 1 1-1 1 1 0 0 1-1 1z"></path></g></g></svg>Information</button>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            <div className="middle" id="scroll">
              <div className="container">
                <ul>
                  {chats.length ? (
                    chats.map((item) => { return <Messages key={item._id} message={item} id={id} user={userId} />})
                  ) : (
                    <li style={{ justifyContent: "center" }}>
                      <i
                        style={{ fontSize: 50, color: "#aaa" }}
                        className="fal fa-comment-alt-lines"
                      ></i>
                    </li>
                  )}
                  <AlwaysScrollToBottom />
                </ul>
              </div>
            </div>
            <MessageInput direct={id} user={userId} socket={socket} />
          </div>
          <div className="utility">
            <div className="container">
              <button type="button" className="close" data-utility="open">
                <i data-eva="close"></i>
              </button>
              <button
                type="button"
                className="btn primary"
                data-toggle="modal"
                data-target="#compose"
              >
                Add people
              </button>
              <ul className="nav" role="tablist">
                <li>
                  <img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" />
                  <div className="content">
                    <div className="message">
                      <div className="bubble">
                        <p>
                          Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry.
                        </p>
                      </div>
                    </div>
                    <span>07:30am</span>
                  </div>
                </li>
                <li>
                  <div className="content">
                    <div className="message">
                      <div className="bubble">
                        <p>Many desktop publishing packages.</p>
                      </div>
                    </div>
                    <span>11:56am</span>
                  </div>
                </li>
                <li>
                  <img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" />
                  <div className="content">
                    <div className="message">
                      <div className="bubble">
                        <div className="attachment">
                          <a href="#" className="round">
                            <i data-eva="file-text"></i>
                          </a>
                          <div className="meta">
                            <a href="#">
                              <h5>build-plugins.js</h5>
                            </a>
                            <span>3kb</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <span>01:03pm</span>
                  </div>
                </li>
                <li>
                  <img src="/dist/img/avatars/avatar-male-3.jpg" alt="avatar" />
                  <div className="content">
                    <div className="message">
                      <div className="bubble">
                        <div className="attachment">
                          <a href="#" className="round">
                            <i data-eva="file-text"></i>
                          </a>
                          <div className="meta">
                            <a href="#">
                              <h5>build-plugins.js</h5>
                            </a>
                            <span>3kb</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <span>01:03pm</span>
                  </div>
                </li>
                <li>
                  <a
                    href="#users"
                    className="active"
                    data-toggle="tab"
                    role="tab"
                    aria-controls="users"
                    aria-selected="true"
                  >
                    Users
                  </a>
                </li>
                <li>
                  <a
                    href="#files"
                    data-toggle="tab"
                    role="tab"
                    aria-controls="files"
                    aria-selected="false"
                  >
                    Files
                  </a>
                </li>
              </ul>
              <div className="tab-content">
                <div
                  className="tab-pane fade active show"
                  id="users"
                  role="tabpanel"
                >
                  <h4>Users</h4>
                  <hr />
                  <ul className="users">
                    <li>
                      <div className="status online">
                        <img
                          src="/dist/img/avatars/avatar-male-1.jpg"
                          alt="avatar"
                        />
                        <i data-eva="radio-button-on"></i>
                      </div>
                      <div className="content">
                        <h5>Ham Chuwon</h5>
                        <span>Florida, US</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="status offline">
                        <img
                          src="/dist/img/avatars/avatar-male-2.jpg"
                          alt="avatar"
                        />
                        <i data-eva="radio-button-on"></i>
                      </div>
                      <div className="content">
                        <h5>Quincy Hensen</h5>
                        <span>Shanghai, China</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="status online">
                        <img
                          src="/dist/img/avatars/avatar-male-3.jpg"
                          alt="avatar"
                        />
                        <i data-eva="radio-button-on"></i>
                      </div>
                      <div className="content">
                        <h5>Mark Hog</h5>
                        <span>Olso, Norway</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="status offline">
                        <img
                          src="/dist/img/avatars/avatar-male-4.jpg"
                          alt="avatar"
                        />
                        <i data-eva="radio-button-on"></i>
                      </div>
                      <div className="content">
                        <h5>Sanne Viscaal</h5>
                        <span>Helena, Montana</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="status offline">
                        <img
                          src="/dist/img/avatars/avatar-male-5.jpg"
                          alt="avatar"
                        />
                        <i data-eva="radio-button-on"></i>
                      </div>
                      <div className="content">
                        <h5>Alex Just</h5>
                        <span>London, UK</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="status online">
                        <img
                          src="/dist/img/avatars/avatar-male-6.jpg"
                          alt="avatar"
                        />
                        <i data-eva="radio-button-on"></i>
                      </div>
                      <div className="content">
                        <h5>Arturo Thomas</h5>
                        <span>Vienna, Austria</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>

                <div className="tab-pane fade" id="files" role="tabpanel">
                  <h4>Files</h4>
                  <div className="upload">
                    <label>
                      <input type="file" />
                      <span>Drag & drop files here</span>
                    </label>
                  </div>
                  <ul className="files">
                    <li>
                      <ul className="avatars">
                        <li>
                          <button className="btn round">
                            <i data-eva="file-text"></i>
                          </button>
                        </li>
                        <li>
                          <a href="#">
                            <img
                              src="/dist/img/avatars/avatar-male-1.jpg"
                              alt="avatar"
                            />
                          </a>
                        </li>
                      </ul>
                      <div className="meta">
                        <a href="#">
                          <h5>workbox.js</h5>
                        </a>
                        <span>2kb</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <ul className="avatars">
                        <li>
                          <button className="btn round">
                            <i data-eva="folder"></i>
                          </button>
                        </li>
                        <li>
                          <a href="#">
                            <img
                              src="/dist/img/avatars/avatar-male-2.jpg"
                              alt="avatar"
                            />
                          </a>
                        </li>
                      </ul>
                      <div className="meta">
                        <a href="#">
                          <h5>bug_report</h5>
                        </a>
                        <span>1kb</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <ul className="avatars">
                        <li>
                          <button className="btn round">
                            <i data-eva="briefcase"></i>
                          </button>
                        </li>
                        <li>
                          <a href="#">
                            <img
                              src="/dist/img/avatars/avatar-male-3.jpg"
                              alt="avatar"
                            />
                          </a>
                        </li>
                      </ul>
                      <div className="meta">
                        <a href="#">
                          <h5>nuget.zip</h5>
                        </a>
                        <span>7mb</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <ul className="avatars">
                        <li>
                          <button className="btn round">
                            <i data-eva="image-2"></i>
                          </button>
                        </li>
                        <li>
                          <a href="#">
                            <img
                              src="/dist/img/avatars/avatar-male-4.jpg"
                              alt="avatar"
                            />
                          </a>
                        </li>
                      </ul>
                      <div className="meta">
                        <a href="#">
                          <h5>clearfix.jpg</h5>
                        </a>
                        <span>1kb</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <ul className="avatars">
                        <li>
                          <button className="btn round">
                            <i data-eva="folder"></i>
                          </button>
                        </li>
                        <li>
                          <a href="#">
                            <img
                              src="/dist/img/avatars/avatar-male-5.jpg"
                              alt="avatar"
                            />
                          </a>
                        </li>
                      </ul>
                      <div className="meta">
                        <a href="#">
                          <h5>package</h5>
                        </a>
                        <span>4mb</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <ul className="avatars">
                        <li>
                          <button className="btn round">
                            <i data-eva="file-text"></i>
                          </button>
                        </li>
                        <li>
                          <a href="#">
                            <img
                              src="/dist/img/avatars/avatar-male-6.jpg"
                              alt="avatar"
                            />
                          </a>
                        </li>
                      </ul>
                      <div className="meta">
                        <a href="#">
                          <h5>plugins.js</h5>
                        </a>
                        <span>3kb</span>
                      </div>
                      <div className="dropdown">
                        <button
                          type="button"
                          className="btn"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i data-eva="more-vertical"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                          <button type="button" className="dropdown-item">
                            Edit
                          </button>
                          <button type="button" className="dropdown-item">
                            Share
                          </button>
                          <button type="button" className="dropdown-item">
                            Delete
                          </button>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div style={{ margin: "20% 47%" }}>
        <Loader type="Oval" color="#00BFFF" height={100} width={100} />
      </div>
    );
  }
}
