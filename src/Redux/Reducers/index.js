import { combineReducers } from "redux";

import users from "./users";
import user from "./user";
import login from "./login";
import verify from "./verify";
import settings from "./settings";

export default combineReducers({ users, user, login, verify, settings });
