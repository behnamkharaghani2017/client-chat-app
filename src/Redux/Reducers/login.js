import loginActions from "../Actions/login";

const loginState = {
  status :false,
  login:[]
};
const login = (state = loginState, action) => {
  console.log('reducer',state)
  // let newState = state;
  switch (action.type) {
    case loginActions.LOGIN_REQUEST:
      return {
        ...state,
        status :false,
      }
    case loginActions.LOGIN_SUCCESS:
      console.log('reducer222',action.payload)
      return {
        ...state,
        status :true,
        login : action.payload
      }
    case loginActions.LOGIN_FAILED:
      return {
        ...state,
        isLoader :false,
        login : action.payload
      }
    default:
      return state;
  }
  // return newState;
};

export default login;
