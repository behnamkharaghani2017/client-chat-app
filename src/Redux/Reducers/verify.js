import verifyActions from "../Actions/verify";

const verifyState = {
  status :false,
  verify:[],
};
const verify = (state = verifyState, action) => {
  console.log('reducer',state)
  // let newState = state;
  switch (action.type) {
    case verifyActions.VERIFY_REQUEST:
      return {
        ...state,
        status :false,
      }
    case verifyActions.VERIFY_SUCCESS:
      console.log('reducer222',action.payload)
      return {
        ...state,
        status :true,
        verify : action.payload
      }
    case verifyActions.VERIFY_FAILED:
      return {
        ...state,
        isLoader :false,
        verify : action.payload
      }
    default:
      return state;
  }
  // return newState;
};

export default verify;
