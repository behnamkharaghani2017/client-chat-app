import userActions from "../Actions/user";

const userState = {
  isLoader :false,
  user:[]
};
const user = (state = userState, action) => {
  console.log('reducer',state)
  // let newState = state;
  switch (action.type) {
    case userActions.USER_REQUEST:
      return {
        ...state,
        isLoader :true,
      }
    case userActions.USER_SUCCESS:
      console.log('reducer222',action.payload)
      return {
        ...state,
        isLoader :false,
        user : action.payload
      }
    case userActions.USER_FAILED:
      return {
        ...state,
        isLoader :false,
        user : action.payload
      }
    default:
      return state;
  }
  // return newState;
};

export default user;
