import settingsActions from "../Actions/settings";

const settingsState = {
  isLoader :false,
  settings:[]
};
const settings = (state = settingsState, action) => {
  console.log('settingsstate',state)
  // let newState = state;
  switch (action.type) {
    case settingsActions.SETTINGS_REQUEST:
      return {
        ...state,
        isLoader :true,
      }
    case settingsActions.SETTINGS_SUCCESS:
      console.log('settingsstate222',action.payload)
      return {
        ...state,
        isLoader :false,
        settings : action.payload
      }
    case settingsActions.SETTINGS_FAILED:
      return {
        ...state,
        isLoader :false,
        settings : action.payload
      }
    default:
      return state;
  }
  // return newState;
};

export default settings;
