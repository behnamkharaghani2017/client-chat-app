import userActions from "../Actions/users";

const usersState = {
  isLoader :false,
  users:[]
};
const users = (state = usersState, action) => {
  console.log('reducer',state)
  // let newState = state;
  switch (action.type) {
    case userActions.USERS_REQUEST:
      return {
        ...state,
        isLoader :true,
      }
    case userActions.USERS_SUCCESS:
      console.log('reducer222',action.payload)
      return {
        ...state,
        isLoader :false,
        users : action.payload
      }
    case userActions.USERS_FAILED:
      return {
        ...state,
        isLoader :false,
        users : action.payload
      }
    default:
      return state;
  }
  // return newState;
};

export default users;
