export default {
    SETTINGS_REQUEST: "SETTINGS_REQUEST",
    SETTINGS_SUCCESS: "SETTINGS_SUCCESS",
    SETTINGS_FAILED: "SETTINGS_FAILED"
}