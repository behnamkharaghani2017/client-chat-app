import { takeLatest, call, put } from "redux-saga/effects";
import pvActions from "../Actions/pv";
import httpService from '../../services/httpService'
export const pvWorker = function* (action) {
  try {
    const user = yield call(() => {
      const httpServ = new httpService()
      return httpServ.post('v1/pv',action.payload)
    });
    yield put({ type: pvActions.USER_PV_SUCCESS, payload: user.data });
  } catch (error) {
    yield put({ type: pvActions.USER_PV_FAILED, payload: error });
  }
};
export const pvWatcher = function* () {
  yield takeLatest(pvActions.USER_PV, pvWorker);
};
