import { takeLatest, call, put } from "redux-saga/effects";
import verifyActions from "../Actions/verify";
import httpService from '../../services/httpService'
export const verifyWorker = function* (action) {
  console.log('saga',action.type)
  try {
    const verify = yield call(() => {
      const httpServ = new httpService()
      return httpServ.post('v1/auth/verify',action.payload)
    });
    console.log('verify',verify.data)
    localStorage.setItem("token", verify.data.token);
    localStorage.setItem("user", verify.data.id);
    yield put({ type: verifyActions.VERIFY_SUCCESS, payload: verify.data });
  } catch (error) {
    yield put({ type: verifyActions.VERIFY_FAILED, payload: error });
  }
};
export const verifyWatcher = function* () {
  yield takeLatest(verifyActions.VERIFY_REQUEST, verifyWorker);
};
