import { all } from "redux-saga/effects";

import * as usersHandler from './users'
import * as userHandler from './user'
import * as loginHandler from './login'
import * as verifyHandler from './verify'
import * as settingsHandler from './settings'

export default function* root(){
    yield all([
        usersHandler.userRegisterWatcher(),
        userHandler.userDataWatcher(),
        loginHandler.loginWatcher(),
        verifyHandler.verifyWatcher(),
        settingsHandler.settingsRegisterWatcher()
    ])
}