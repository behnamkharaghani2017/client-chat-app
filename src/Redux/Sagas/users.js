import { takeLatest, call, put } from "redux-saga/effects";
import usersActions from "../Actions/users";
import httpService from '../../services/httpService'
export const usersRegisterWorker = function* (action) {
  try {
    const users = yield call(() => {
      const httpServ = new httpService()
      return httpServ.get('v1/users')
    });
    yield put({ type: usersActions.USERS_SUCCESS, payload: users.data });
  } catch (error) {
    yield put({ type: usersActions.USERS_FAILED, payload: error });
  }
};
export const userRegisterWatcher = function* () {
  yield takeLatest(usersActions.USERS_REQUEST, usersRegisterWorker);
};
