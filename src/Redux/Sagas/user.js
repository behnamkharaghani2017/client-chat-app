import { takeLatest, call, put } from "redux-saga/effects";
import userActions from "../Actions/user";
import httpService from '../../services/httpService'
export const userDataWorker = function* (action) {
  console.log('saga',action.type)
  try {
    const user = yield call(() => {
      const httpServ = new httpService()
      return httpServ.post('v1/user',action.payload)
    });
    console.log('user',user.data)
    yield put({ type: userActions.USER_SUCCESS, payload: user.data });
  } catch (error) {
    yield put({ type: userActions.USER_FAILED, payload: error });
  }
};
export const userDataWatcher = function* () {
  yield takeLatest(userActions.USER_REQUEST, userDataWorker);
};
