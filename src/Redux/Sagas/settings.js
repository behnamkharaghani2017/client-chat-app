import { takeLatest, call, put } from "redux-saga/effects";
import settingsActions from "../Actions/settings";
import httpService from '../../services/httpService'
export const settingsRegisterWorker = function* (action) {
  console.log('saga',action.type)
  try {
    const settings = yield call(() => {
      const httpServ = new httpService()
      return httpServ.post('v1/settings',action.payload)
    });
    console.log('settingstttt',settings.data)
    yield put({ type: settingsActions.SETTINGS_SUCCESS, payload: settings.data });
  } catch (error) {
    yield put({ type: settingsActions.SETTINGS_FAILED, payload: error });
  }
};
export const settingsRegisterWatcher = function* () {
  console.log('saga Watcher')
  yield takeLatest(settingsActions.SETTINGS_REQUEST, settingsRegisterWorker);
};