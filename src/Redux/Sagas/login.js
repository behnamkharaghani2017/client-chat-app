import { takeLatest, call, put } from "redux-saga/effects";
import loginActions from "../Actions/login";
import httpService from '../../services/httpService'
export const loginWorker = function* (action) {
  console.log('saga',action.type)
  try {
    const login = yield call(() => {
      const httpServ = new httpService()
      return httpServ.post('v1/auth/login',action.payload)
    });
    console.log('login',login.data)
    yield put({ type: loginActions.LOGIN_SUCCESS, payload: login.data });
  } catch (error) {
    yield put({ type: loginActions.LOGIN_FAILED, payload: error });
  }
};
export const loginWatcher = function* () {
  yield takeLatest(loginActions.LOGIN_REQUEST, loginWorker);
};
